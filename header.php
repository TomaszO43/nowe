<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="https://fonts.googleapis.com/css?family=Lato:300,700,900&amp;subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet/less" type="text/css" href="./css/reset.less">
    <link rel="stylesheet/less" type="text/css" href="./css/promo.less">
    <link rel="stylesheet/less" type="text/css" href="./css/common.less">
    <link rel="stylesheet/less" type="text/css" href="./css/style-mobile.less">
    <link rel="stylesheet/less" type="text/css" href="./css/artticle.less">
    <link rel="stylesheet/less" type="text/css" href="./css/prod.less">




    <script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
    <script src="./lib/less.js" type="text/javascript"></script>
    <title>Title</title>
    <link rel="stylesheet" type="text/css" href="./lib/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="./lib/slick/slick-theme.css"/>

</head>
<body>

<header>
    <div class="social">
        <a href="">
            <i class="fab fa-linkedin-in"></i>
        </a>
        <a href="">
            <i class="fab fa-instagram"></i>
        </a>
        <a href="">
            <i class="fab fa-twitter"></i>
        </a>
        <a href="">
            <i class="fab fa-facebook-f"></i>
        </a>
    </div>
    <div class="languages">
        <ul>
            <li><a href="" title="PL" class="active">PL</a></li>
            <li><a href="" title="EN">EN</a></li>
        </ul>
    </div>

    <div class="menu" id="menu">
        <div id="menuicon">
            <span class="rotate"></span>
            <span></span>
            <span class="rotate1"></span>
        </div>
        <a href="" class="logo">
            <img src="./img/logo.png" alt="Pure" />
        </a>

        <nav >
            <ul>
                <li>
                    <a href="/prod.php" class="m-menu">pRoDuKty</a>
                </li>
                <li>
                    <a href="/promocje.php" class="m-menu">PROMOCJE</a>
                </li>
                <li class="logo_button">
                    <a href="/" >
                        <img src="./img/logo.png" alt="Pure"/>
                    </a>
                </li>
                <li>
                    <a href="/porady.php" class="m-menu">PORADY</a>
                </li>
                <li>
                    <a href="/contact.php" class="m-menu">KONTAKT</a>
                </li>
            </ul>
        </nav>
    </div>
</header>